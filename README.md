# README #

Hi, Good day.

This repo contains one solution with four projects.

####Project NPV.Api####
NPV.Api is a simple API that saves the calculation of the Net Pet Present Value Calculator and also returns the saved calculations of any callers to the API in a SQLite database.

####Project NPV.Angular.Web####
NPV.Angular.Web is a simple website that uses JavaScript/AngularJS 7 to call a controller's action that calculates the Net Present Value at the push of a button that displays and saves the resulting JSON object.

####Project NPV.BAL####
NPV.BAL is a simple .Net Core Class Library that contains the business domain.

####Project NPV.Models####
NPV.Models is a simple .Net Core Class Library that contains models that can be used across projects.

####Required Technologies####
* Node.js.
* A windows or Mac computer.
* Visual Studio Community Edition (latest version).  Note there are missing components on the Mac that you will have to self install (NuGet CLI) to run the API project while running the website.

### How do I get set up? ###

* Install Visual Studio Community 2017 or Visual Studio Code if you do not have it set up.  Note that Visual Studio 2015 will not work with this project.
* [Install Node.js](https://nodejs.org/en/download/)
* Clone this repo.
* Open a Comman Prompt (cmd), go to NPV\NPV.Angular.Web\ClientApp project folder then type 'npm install' to install node_modules and packages needed for the AngularJS 7 project, make sure that the installation is successful.
* Once the npm installation is done, type 'ng build' and make sure the build is susccessful.
* Change local configurations that may need changed for your environment.
* Check CORS access.
* Since this is a code first project you need to run migrations to generate the database. You can use Update-Database command in the Package Manager Console targeting the NPV.Api project.


### Who do I talk to? ###

* For this repo, questions on direction, scope, or intent, please send an email to jefersondeguzman8@gmail.com.

### Additional Resources ###
[Getting Started with EF Core on .NET Core Console App with a New database](https://docs.microsoft.com/en-us/ef/core/get-started/netcore/new-db-sqlite)

[EF Core .NET Command-line Tools](https://docs.microsoft.com/en-us/ef/core/miscellaneous/cli/dotnet)

[Migrations - EF Core with ASP.NET Core MVC](https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/migrations#introduction-to-migrations)
