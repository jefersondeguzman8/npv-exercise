﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using NPV.Api.Entities;

namespace NPV.Api.Migrations
{
    [DbContext(typeof(NetPresentValueContext))]
    partial class NetPresentValueContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024");

            modelBuilder.Entity("NPV.Api.Entities.NetPresentValue", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("CashFlowYear1");

                    b.Property<double>("CashFlowYear10");

                    b.Property<double>("CashFlowYear11");

                    b.Property<double>("CashFlowYear12");

                    b.Property<double>("CashFlowYear13");

                    b.Property<double>("CashFlowYear14");

                    b.Property<double>("CashFlowYear15");

                    b.Property<double>("CashFlowYear16");

                    b.Property<double>("CashFlowYear17");

                    b.Property<double>("CashFlowYear18");

                    b.Property<double>("CashFlowYear19");

                    b.Property<double>("CashFlowYear2");

                    b.Property<double>("CashFlowYear20");

                    b.Property<double>("CashFlowYear3");

                    b.Property<double>("CashFlowYear4");

                    b.Property<double>("CashFlowYear5");

                    b.Property<double>("CashFlowYear6");

                    b.Property<double>("CashFlowYear7");

                    b.Property<double>("CashFlowYear8");

                    b.Property<double>("CashFlowYear9");

                    b.Property<DateTime>("CreationDateTime");

                    b.Property<double>("DiscountRate");

                    b.Property<double>("DiscountRateIncrement");

                    b.Property<double>("InitialCost");

                    b.Property<short>("LifeOfProject");

                    b.Property<double>("LowerBoundDiscountRate");

                    b.Property<double>("PresentValue");

                    b.Property<double>("UpperBoundDiscountRate");

                    b.Property<double>("Value");

                    b.HasKey("Id");

                    b.ToTable("NetPresentValues");
                });
#pragma warning restore 612, 618
        }
    }
}
