﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NPV.Api.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NetPresentValues",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DiscountRate = table.Column<double>(nullable: false),
                    LowerBoundDiscountRate = table.Column<double>(nullable: false),
                    UpperBoundDiscountRate = table.Column<double>(nullable: false),
                    DiscountRateIncrement = table.Column<double>(nullable: false),
                    LifeOfProject = table.Column<short>(nullable: false),
                    InitialCost = table.Column<double>(nullable: false),
                    CashFlowYear1 = table.Column<double>(nullable: false),
                    CashFlowYear2 = table.Column<double>(nullable: false),
                    CashFlowYear3 = table.Column<double>(nullable: false),
                    CashFlowYear4 = table.Column<double>(nullable: false),
                    CashFlowYear5 = table.Column<double>(nullable: false),
                    CashFlowYear6 = table.Column<double>(nullable: false),
                    CashFlowYear7 = table.Column<double>(nullable: false),
                    CashFlowYear8 = table.Column<double>(nullable: false),
                    CashFlowYear9 = table.Column<double>(nullable: false),
                    CashFlowYear10 = table.Column<double>(nullable: false),
                    CashFlowYear11 = table.Column<double>(nullable: false),
                    CashFlowYear12 = table.Column<double>(nullable: false),
                    CashFlowYear13 = table.Column<double>(nullable: false),
                    CashFlowYear14 = table.Column<double>(nullable: false),
                    CashFlowYear15 = table.Column<double>(nullable: false),
                    CashFlowYear16 = table.Column<double>(nullable: false),
                    CashFlowYear17 = table.Column<double>(nullable: false),
                    CashFlowYear18 = table.Column<double>(nullable: false),
                    CashFlowYear19 = table.Column<double>(nullable: false),
                    CashFlowYear20 = table.Column<double>(nullable: false),
                    Value = table.Column<double>(nullable: false),
                    PresentValue = table.Column<double>(nullable: false),
                    CreationDateTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NetPresentValues", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NetPresentValues");
        }
    }
}
