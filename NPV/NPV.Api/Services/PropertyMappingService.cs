﻿using NPV.Api.Entities;
using NPV.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NPV.Api.Services
{
    public class PropertyMappingService : IPropertyMappingService
    {
        private Dictionary<string, PropertyMappingValue> _netPresentValuePropertyMapping =
           new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
           {
               { "DiscountRate", new PropertyMappingValue(new List<string>() { "DiscountRate" } ) },
               { "LowerBoundDiscountRate", new PropertyMappingValue(new List<string>() { "LowerBoundDiscountRate" } )},
               { "UpperBoundDiscountRate", new PropertyMappingValue(new List<string>() { "UpperBoundDiscountRate" }) },
               { "DiscountRateIncrement", new PropertyMappingValue(new List<string>() { "DiscountRateIncrement" }) },
               { "LifeOfProject", new PropertyMappingValue(new List<string>() { "LifeOfProject" }) },
               { "InitialCost", new PropertyMappingValue(new List<string>() { "InitialCost" } ) },
               { "CashFlowYear1", new PropertyMappingValue(new List<string>() { "CashFlowYear1" } )},
               { "CashFlowYear2", new PropertyMappingValue(new List<string>() { "CashFlowYear2" }) },
               { "CashFlowYear3", new PropertyMappingValue(new List<string>() { "CashFlowYear3" }) },
               { "CashFlowYear4", new PropertyMappingValue(new List<string>() { "CashFlowYear4" }) },
               { "CashFlowYear5", new PropertyMappingValue(new List<string>() { "CashFlowYear5" }) },
               { "CashFlowYear6", new PropertyMappingValue(new List<string>() { "CashFlowYear6" }) },
               { "CashFlowYear7", new PropertyMappingValue(new List<string>() { "CashFlowYear7" } )},
               { "CashFlowYear8", new PropertyMappingValue(new List<string>() { "CashFlowYear8" }) },
               { "CashFlowYear9", new PropertyMappingValue(new List<string>() { "CashFlowYear9" }) },
               { "CashFlowYear10", new PropertyMappingValue(new List<string>() { "CashFlowYear10" }) },
               { "CashFlowYear11", new PropertyMappingValue(new List<string>() { "CashFlowYear11" } )},
               { "CashFlowYear12", new PropertyMappingValue(new List<string>() { "CashFlowYear12" }) },
               { "CashFlowYear13", new PropertyMappingValue(new List<string>() { "CashFlowYear13" }) },
               { "CashFlowYear14", new PropertyMappingValue(new List<string>() { "CashFlowYear14" }) },
               { "CashFlowYear15", new PropertyMappingValue(new List<string>() { "CashFlowYear15" }) },
               { "CashFlowYear16", new PropertyMappingValue(new List<string>() { "CashFlowYear16" }) },
               { "CashFlowYear17", new PropertyMappingValue(new List<string>() { "CashFlowYear17" }) },
               { "CashFlowYear18", new PropertyMappingValue(new List<string>() { "CashFlowYear18" }) },
               { "CashFlowYear19", new PropertyMappingValue(new List<string>() { "CashFlowYear19" }) },
               { "CashFlowYear20", new PropertyMappingValue(new List<string>() { "CashFlowYear20" }) },
               { "Value", new PropertyMappingValue(new List<string>() { "Value" }) },
               { "PresentValue", new PropertyMappingValue(new List<string>() { "PresentValue" }) },
               { "CreationDateTime", new PropertyMappingValue(new List<string>() { "CreationDateTime" }) }
        };

        private IList<IPropertyMapping> propertyMappings = new List<IPropertyMapping>();

        public PropertyMappingService()
        {
            propertyMappings.Add(new PropertyMapping<NetPresentValueDto, NetPresentValue>(_netPresentValuePropertyMapping));
        }
        public Dictionary<string, PropertyMappingValue> GetPropertyMapping
            <TSource, TDestination>()
        {
            // get matching mapping
            var matchingMapping = propertyMappings.OfType<PropertyMapping<TSource, TDestination>>();

            if (matchingMapping.Count() == 1)
            {
                return matchingMapping.First()._mappingDictionary;
            }

            throw new Exception($"Cannot find exact property mapping instance for <{typeof(TSource)},{typeof(TDestination)}");
        }

        public bool ValidMappingExistsFor<TSource, TDestination>(string fields)
        {
            var propertyMapping = GetPropertyMapping<TSource, TDestination>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                return true;
            }

            // the string is separated by ",", so we split it.
            var fieldsAfterSplit = fields.Split(',');

            // run through the fields clauses
            foreach (var field in fieldsAfterSplit)
            {
                // trim
                var trimmedField = field.Trim();

                // remove everything after the first " " - if the fields 
                // are coming from an orderBy string, this part must be 
                // ignored
                var indexOfFirstSpace = trimmedField.IndexOf(" ");
                var propertyName = indexOfFirstSpace == -1 ?
                    trimmedField : trimmedField.Remove(indexOfFirstSpace);

                // find the matching property
                if (!propertyMapping.ContainsKey(propertyName))
                {
                    return false;
                }
            }

            return true;
        }

    }
}

