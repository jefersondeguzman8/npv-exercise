﻿using NPV.Api.Entities;
using NPV.Api.Helpers;
using NVP.Api.Helpers;
using System;
using System.Threading.Tasks;

namespace NPV.Api.Repositories
{
    public interface INetPresentValueRepository
    {
        Task AddNetPresentValue(NetPresentValue netPresentValue);
        Task<PagedList<NetPresentValue>> GetNetPresentValues(NetPresentValueResourcesParameters netPresentValueResourcesParameters);
        Task<NetPresentValue> GetNetPresentValue(Guid netPresentValueId);
        Task<bool> Save();
    }
}
