﻿using Microsoft.EntityFrameworkCore;
using NPV.Api.Entities;
using NPV.Api.Helpers;
using NPV.Api.Models;
using NPV.Api.Services;
using NVP.Api.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NPV.Api.Repositories
{
    public class NetPresentValueRepository : INetPresentValueRepository
    {
        private NetPresentValueContext _context;
        private IPropertyMappingService _propertyMappingService;

        public NetPresentValueRepository(NetPresentValueContext context,
            IPropertyMappingService propertyMappingService)
        {
            _context = context;
            _propertyMappingService = propertyMappingService;
        }

        public async Task AddNetPresentValue(NetPresentValue netPresentValue)
        {
            netPresentValue.Id = Guid.NewGuid();
            await _context.NetPresentValues.AddAsync(netPresentValue);
        }

        public async Task<NetPresentValue> GetNetPresentValue(Guid netPresentValueId)
        {
            return await _context.NetPresentValues.FirstOrDefaultAsync(a => a.Id == netPresentValueId);
        }

        public async Task<PagedList<NetPresentValue>> GetNetPresentValues(
            NetPresentValueResourcesParameters netPresentValueResourcesParameters)
        {
            var collectionBeforePaging =
                await _context.NetPresentValues.ApplySort(netPresentValueResourcesParameters.OrderBy,
                _propertyMappingService.GetPropertyMapping<NetPresentValueDto, NetPresentValue>())
                .ToListAsync();

            var netPresentValues = PagedList<NetPresentValue>.Create(collectionBeforePaging.AsQueryable(),
                netPresentValueResourcesParameters.PageNumber,
                netPresentValueResourcesParameters.PageSize);

            return netPresentValues;
        }

        public async Task<bool> Save()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }
    }
}

