﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPV.Api.Entities;
using NPV.Api.Helpers;
using NPV.Api.Models;
using NPV.Api.Repositories;
using NPV.Api.Services;
using NPV.Api.Validations;
using NVP.Api.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NPV.Api.Controllers
{
    [Route("api/[controller]")]
    public class NetPresentValuesController : Controller
    {
        private INetPresentValueRepository _netPresentValueRepository;
        private IUrlHelper _urlHelper;
        private IPropertyMappingService _propertyMappingService;
        private IHttpContextAccessor _accessor;
        private ITypeHelperService _typeHelperService;

        public NetPresentValuesController(INetPresentValueRepository netPresentValueRepository,
            IUrlHelper urlHelper,
            IPropertyMappingService propertyMappingService,
            IHttpContextAccessor accessor,
            ITypeHelperService typeHelperService)
        {
            _netPresentValueRepository = netPresentValueRepository;
            _urlHelper = urlHelper;
            _propertyMappingService = propertyMappingService;
            _accessor = accessor;
            _typeHelperService = typeHelperService;
        }

        private const string GetNetPresentValueMethodName = "GetNetPresentValue";
        private const string GetNetPresentValuesMethodName = "GetNetPresentValues";
        private const string CreateNetPresentValueMethodName = "CreateNetPresentValue";

        [HttpGet("{id}", Name = GetNetPresentValueMethodName)]
        public async Task<IActionResult> GetNetPresentValueAsync(Guid id, [FromQuery] string fields)
        {
            if (!_typeHelperService.TypeHasProperties<NetPresentValueDto>(fields))
            {
                return BadRequest();
            }

            var netPresentValueFromRepo = await _netPresentValueRepository.GetNetPresentValue(id);

            if (netPresentValueFromRepo == null)
            {
                return NotFound();
            }

            var netPresentValue = Mapper.Map<NetPresentValueDto>(netPresentValueFromRepo);

            var links = CreateLinksForNetPresentValue(id, fields);

            var linkedResourceToReturn = netPresentValue.ShapeData(fields)
                as IDictionary<string, object>;

            linkedResourceToReturn.Add("links", links);

            return Ok(linkedResourceToReturn);
        }

        [HttpGet(Name = GetNetPresentValuesMethodName)]
        public async Task<IActionResult> GetNetPresentValuesAsync(NetPresentValueResourcesParameters netPresentValueResourcesParameters)
        {
            if (!_propertyMappingService.ValidMappingExistsFor<NetPresentValueDto, NetPresentValue>(netPresentValueResourcesParameters.OrderBy))
            {
                ModelState.AddModelError(nameof(netPresentValueResourcesParameters.OrderBy), Messages.InvalidOrderBy);
                return BadRequest();
            }

            if (!_typeHelperService.TypeHasProperties<NetPresentValueDto>(netPresentValueResourcesParameters.Fields))
            {
                ModelState.AddModelError(nameof(netPresentValueResourcesParameters.Fields), Messages.InvalidFields);
                return BadRequest();
            }

            var netPresentValueFromRepo = await _netPresentValueRepository.GetNetPresentValues(netPresentValueResourcesParameters);

            var netPresentValues = Mapper.Map<IEnumerable<NetPresentValueDto>>(netPresentValueFromRepo);

            var paginationMetadata = new
            {
                totalCount = netPresentValueFromRepo.TotalCount,
                pageSize = netPresentValueFromRepo.PageSize,
                currentPage = netPresentValueFromRepo.CurrentPage,
                totalPages = netPresentValueFromRepo.TotalPages
            };

            Response.Headers.Add("X-Pagination",
                Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetadata));

            var links = CreateLinksForNetPresentValues(netPresentValueResourcesParameters,
                netPresentValueFromRepo.HasNext, netPresentValueFromRepo.HasPrevious);

            var shapedNetPresentValues = netPresentValues.ShapeData(netPresentValueResourcesParameters.Fields);

            var linkedNetPresentValueCollectionResourceDto = new LinkedNetPresentValueCollectionResourceDto()
            {
                Value = shapedNetPresentValues,
                Links = links
            };

            return Ok(linkedNetPresentValueCollectionResourceDto);
        }

        [HttpPost("create-net-present-value", Name = CreateNetPresentValueMethodName)]
        public async Task<IActionResult> CreateNetPresentValueAsync([FromBody] NetPresentValueForCreationDto netPresentValue)
        {
            if (netPresentValue == null)
            {
                return BadRequest();
            }

            var netPresentValueEntity = Mapper.Map<NetPresentValue>(netPresentValue);

            netPresentValueEntity.CreationDateTime = DateTime.Now;
            await _netPresentValueRepository.AddNetPresentValue(netPresentValueEntity);

            if (!(await _netPresentValueRepository.Save()))
            {
                return StatusCode(500, "A problem happened with handling your request.");
            }

            var netPresentValueToReturn = Mapper.Map<NetPresentValueDto>(netPresentValueEntity);

            var links = CreateLinksForNetPresentValue(netPresentValueToReturn.Id, null);

            var linkedResourceToReturn = netPresentValueToReturn.ShapeData(null)
                as IDictionary<string, object>;

            linkedResourceToReturn.Add("links", links);

            return CreatedAtRoute(GetNetPresentValueMethodName,
                new { id = linkedResourceToReturn["Id"] },
                linkedResourceToReturn);
        }

        [HttpPost("create-net-present-values")]
        public async Task<IActionResult> CreateNetPresentValueCollectionAsync(
            [FromBody] IEnumerable<NetPresentValueForCreationDto> netPresentValueCollection)
        {
            if (netPresentValueCollection == null)
            {
                return BadRequest();
            }

            var netPresentValueEntities = Mapper.Map<IEnumerable<NetPresentValue>>(netPresentValueCollection);

            foreach (var netPresentValue in netPresentValueEntities)
            {
                netPresentValue.CreationDateTime = DateTime.Now;
                await _netPresentValueRepository.AddNetPresentValue(netPresentValue);
            }

            if (!(await _netPresentValueRepository.Save()))
            {
                throw new Exception("Creating an net present value collection failed on save.");
            }

            // TODO
            var netPresentValueCollectionToReturn = Mapper.Map<IEnumerable<NetPresentValueDto>>(netPresentValueEntities);
            return Ok(netPresentValueCollectionToReturn);
        }

        private IEnumerable<LinkDto> CreateLinksForNetPresentValue(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link(GetNetPresentValueMethodName, new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link(GetNetPresentValueMethodName, new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForNetPresentValues(
            NetPresentValueResourcesParameters netPresentValueResourceParameters,
            bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>
            {
                // self 
                new LinkDto(CreateNetPresentValuesResourceUri(netPresentValueResourceParameters,
                ResourceUriType.Current),
                "self", "GET")
            };

            if (hasNext)
            {
                links.Add(
                    new LinkDto(CreateNetPresentValuesResourceUri(netPresentValueResourceParameters,
                    ResourceUriType.NextPage),
                    "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(
                    new LinkDto(CreateNetPresentValuesResourceUri(netPresentValueResourceParameters,
                    ResourceUriType.PreviousPage),
                    "previousPage", "GET"));
            }

            return links;
        }

        private string CreateNetPresentValuesResourceUri(
            NetPresentValueResourcesParameters netPresentValueResourceParameters,
            ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link(GetNetPresentValuesMethodName,
                      new
                      {
                          fields = netPresentValueResourceParameters.Fields,
                          orderBy = netPresentValueResourceParameters.OrderBy,
                          pageNumber = netPresentValueResourceParameters.PageNumber - 1,
                          pageSize = netPresentValueResourceParameters.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link(GetNetPresentValuesMethodName,
                      new
                      {
                          fields = netPresentValueResourceParameters.Fields,
                          orderBy = netPresentValueResourceParameters.OrderBy,
                          pageNumber = netPresentValueResourceParameters.PageNumber + 1,
                          pageSize = netPresentValueResourceParameters.PageSize
                      });
                case ResourceUriType.Current:
                default:
                    return _urlHelper.Link(GetNetPresentValuesMethodName,
                    new
                    {
                        fields = netPresentValueResourceParameters.Fields,
                        orderBy = netPresentValueResourceParameters.OrderBy,
                        pageNumber = netPresentValueResourceParameters.PageNumber,
                        pageSize = netPresentValueResourceParameters.PageSize
                    });
            }
        }
    }
}
