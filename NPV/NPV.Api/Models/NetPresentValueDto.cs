﻿using System;

namespace NPV.Api.Models
{
    public class NetPresentValueDto
    {
        public Guid Id { get; set; }
        public double DiscountRate { get; set; }
        public double LowerBoundDiscountRate { get; set; }
        public double UpperBoundDiscountRate { get; set; }
        public double DiscountRateIncrement { get; set; }
        public short LifeOfProject { get; set; }
        public double InitialCost { get; set; }
        public double CashFlowYear1 { get; set; }
        public double CashFlowYear2 { get; set; }
        public double CashFlowYear3 { get; set; }
        public double CashFlowYear4 { get; set; }
        public double CashFlowYear5 { get; set; }
        public double CashFlowYear6 { get; set; }
        public double CashFlowYear7 { get; set; }
        public double CashFlowYear8 { get; set; }
        public double CashFlowYear9 { get; set; }
        public double CashFlowYear10 { get; set; }
        public double CashFlowYear11 { get; set; }
        public double CashFlowYear12 { get; set; }
        public double CashFlowYear13 { get; set; }
        public double CashFlowYear14 { get; set; }
        public double CashFlowYear15 { get; set; }
        public double CashFlowYear16 { get; set; }
        public double CashFlowYear17 { get; set; }
        public double CashFlowYear18 { get; set; }
        public double CashFlowYear19 { get; set; }
        public double CashFlowYear20 { get; set; }
        public double Value { get; set; }
        public double PresentValue { get; set; }
        public DateTime CreationDateTime { get; set; }
    }
}
