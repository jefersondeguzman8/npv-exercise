﻿using System.Collections.Generic;
using System.Dynamic;

namespace NPV.Api.Models
{
    public class LinkedNetPresentValueCollectionResourceDto
    {
        public IEnumerable<ExpandoObject> Value { get; set; }
        public IEnumerable<LinkDto> Links { get; set; }
    }
}
