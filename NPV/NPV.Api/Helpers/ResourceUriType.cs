﻿namespace NVP.Api.Helpers
{
    public enum ResourceUriType
    {
        PreviousPage,
        NextPage,
        Current
    }
}
