﻿using NPV.Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NPV.Api.Entities
{
    public static class NetPresentValueContextExtensions
    {
        public static void EnsureSeedDataForContext(this NetPresentValueContext context)
        {
            // clear init seeded data
            if (context.NetPresentValues.FirstOrDefault(x => x.Id.ToString() == "76053df4-6687-4353-8937-b45556748abe") != null
                || context.NetPresentValues.FirstOrDefault(x => x.Id.ToString() == "412c3012-d891-4f5e-9613-ff7aa63e6bb3") != null)
            {
                context.NetPresentValues.RemoveRange(context.NetPresentValues);
                context.SaveChanges();
            }            
        }
    }
}