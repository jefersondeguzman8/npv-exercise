﻿using Microsoft.EntityFrameworkCore;

namespace NPV.Api.Entities
{
    public class NetPresentValueContext : DbContext
    {
        public NetPresentValueContext(DbContextOptions<NetPresentValueContext> options)
           : base(options)
        {
        }

        public DbSet<NetPresentValue> NetPresentValues { get; set; }
    }
}

