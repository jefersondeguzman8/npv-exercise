﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NPV.Api.Validations
{
    public class Messages
    {
        public static string InvalidOrderBy { get; private set; } = "The order by is invalid.";
        public static string InvalidFields { get; private set; } = "Fields are invalid.";
    }
}
