import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'percentConvertToWholeNumber'
})
export class PercentConvertToWholeNumberPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return (value * 100);
  }

}
