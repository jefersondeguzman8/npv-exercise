import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

import { NetPresentValue } from '../models/netpresentvalue';
import { NetPresentValueParam } from '../models/netpresentvalueparam';
import { NetPresentValueResult } from '../models/NetPresentValueResult';
import { NetPresentValueCollection } from '../models/netpresentvaluecollection';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NetPresentValueService {
  private apiUrl = environment.urlAddress.endsWith('/') ? environment.urlAddress + 'api/netpresentvalues' : environment.urlAddress + '/api/netpresentvalues';
  private netPresentValuesUrl = 'api/netpresentvalues/calculatenetpresentvalue';
  private baseUrl: string = '';

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  calculateNetPresentValue(netPresentValueParam: NetPresentValueParam): Observable<NetPresentValueResult[]> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<NetPresentValueResult[]>(this.baseUrl + this.netPresentValuesUrl, netPresentValueParam, { headers: headers })
      .pipe(
        tap(data => console.log('calculate net present value: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  createNetPresentValues(netpresentValues: NetPresentValue[]): Observable<NetPresentValue[]> {
    const url = `${this.apiUrl}/create-net-present-values`;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<NetPresentValue[]>(url, netpresentValues, { headers: headers })
      .pipe(
        tap(data => console.log('crate net present values: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  getNetPresentValues(pageNumber: number = 20, pageSize: number = 1, orderBy: string = ''): Observable<HttpResponse<NetPresentValueCollection>> {
    const url = `${this.apiUrl}?pagesize=${pageSize}&pagenumber=${pageNumber}&orderby=${orderBy}`;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<NetPresentValueCollection>(url, { headers: headers, observe: 'response' })
      .pipe(
        tap(data => console.log(JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  private handleError(err) {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }
}
