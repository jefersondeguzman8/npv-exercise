export class NetPresentValueResult {
    public discountRate: number;
    public discountFactors: number[];
    public presentValues: number[];
    public value: number;
    public presentValue: number;
    public discountedValues: number[];
}
