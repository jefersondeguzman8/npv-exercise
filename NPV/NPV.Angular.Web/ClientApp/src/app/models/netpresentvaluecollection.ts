import { Link } from './link';

export class NetPresentValueCollection {
    value: Array<any>;
    links: Link[];
    totalCount: number;
  headers: any;
  body: any;
}