export class NetPresentValueParam {
     public lowerBoundDiscountRate: number;
     public upperBoundDiscountRate: number;
     public discountRateIncrement: number;
     public lifeOfProject: number;
     public initialCost: number;
     public cashFlows: number[];
}
