import { Component } from '@angular/core';
import { MatTableDataSource, Sort, PageEvent } from '@angular/material';

import { NetPresentValueService } from '../shared/netpresentvalue.service';

@Component({
  selector: 'app-netpresentvalue-data',
  templateUrl: './netpresentvalue-data.component.html',
  styleUrls: ['./netpresentvalue-data.component.css']
})
export class NetpresentvalueDataComponent {
  pageLength: number = 0;
  pageNumber: number = 1;
  pageSize: number = 10;
  orderBy: string = '';
  errorMessage = '';

  displayedColumns = ['creationDateTime', 'discountRate', 'lowerBoundDiscountRate', 'upperBoundDiscountRate',
    'discountRateIncrement', 'lifeOfProject', 'value', 'presentValue',
    'cashFlowYear1', 'cashFlowYear2', 'cashFlowYear3', 'cashFlowYear4', 'cashFlowYear5',
    'cashFlowYear6', 'cashFlowYear7', 'cashFlowYear8', 'cashFlowYear9', 'cashFlowYear10',
    'cashFlowYear11', 'cashFlowYear12', 'cashFlowYear13', 'cashFlowYear14', 'cashFlowYear15',
    'cashFlowYear16', 'cashFlowYear17', 'cashFlowYear18', 'cashFlowYear19', 'cashFlowYear20'];

  dataSource: MatTableDataSource<any>;

  constructor(private netPresentValueService: NetPresentValueService) {
    this.getNetPresentValues();
  }

  getNetPresentValues(pageNumber: number = this.pageNumber, pageSize: number = this.pageSize, orderBy: string = this.orderBy) {
    this.netPresentValueService.getNetPresentValues(pageNumber, pageSize, orderBy).subscribe(
      resp => {
        let xPagination = resp.headers.get('X-Pagination');
        if (this.dataSource === undefined) {
          this.dataSource = new MatTableDataSource(resp.body.value);
        }
        else {
          this.dataSource.data = resp.body.value;
        }
        this.pageLength = JSON.parse(xPagination)['totalCount'];
      },
      error => this.errorMessage = <any>error);
  }

  pageChange(pageEvent: PageEvent) {
    this.pageNumber = pageEvent.pageIndex + 1;
    this.pageSize = pageEvent.pageSize;
    this.getNetPresentValues();
  }

  sortChange(sort: Sort) {
    this.orderBy = sort.active + ' ' + sort.direction;
    this.getNetPresentValues();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}