import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetpresentvalueDataComponent } from './netpresentvalue-data.component';

describe('NetpresentvalueDataComponent', () => {
  let component: NetpresentvalueDataComponent;
  let fixture: ComponentFixture<NetpresentvalueDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NetpresentvalueDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetpresentvalueDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
