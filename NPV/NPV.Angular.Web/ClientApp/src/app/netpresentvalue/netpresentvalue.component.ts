import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';

import { NetPresentValue } from '../models/netpresentvalue';
import { NetPresentValueParam } from '../models/netpresentvalueparam';
import { NetPresentValueResult } from '../models/netpresentvalueresult';
import { NetPresentValueService } from '../shared/netpresentvalue.service';

import * as Highcharts from 'highcharts';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

function discountRateChecker(c: AbstractControl): { [key: string]: boolean } | null {
  const lowerBoundDiscountRateControl = c.get('lowerBoundDiscountRate');
  const upperBoundDiscountRateControl = c.get('upperBoundDiscountRate');
  const discountRateIncrementControl = c.get('discountRateIncrement');

  if (!lowerBoundDiscountRateControl.pristine && !upperBoundDiscountRateControl.pristine
    && lowerBoundDiscountRateControl.valid
    && parseFloat(lowerBoundDiscountRateControl.value) > parseFloat(upperBoundDiscountRateControl.value)) {
    return { 'uppperRateLessThanLowerRate': true };
  }
  else {
    return null;
  }
}

@Component({
  selector: 'app-netpresentvalue',
  templateUrl: './netpresentvalue.component.html',
  styleUrls: ['./netpresentvalue.component.css']
})
export class NetPresentValueComponent implements OnInit {
  netPresentValueForm: FormGroup;
  showResult: boolean = false;
  netPresentValue: NetPresentValue;
  netPresentValueParam: NetPresentValueParam;
  netPresentValueResults: NetPresentValueResult[];
  errorMessage: string;
  resultCount: number[];
  triggered: boolean;
  positiveNumberRegex: string = `^[+]?([0-9]+(?:[\\.][0-9]*)?|\\.[0-9]+)$`;
  negatativeNumberRegex: string = `^[-]?([0-9]+(?:[\\.][0-9]*)?|\\.[0-9]+)$`;
  numberRegex: string = `^[+-]?([0-9]+(?:[\\.][0-9]*)?|\\.[0-9]+)$`;

  get cashFlows(): FormArray {
    return <FormArray>this.netPresentValueForm.get('cashFlows');
  }

  get results(): FormArray {
    return <FormArray>this.netPresentValueForm.get('results');
  }

  constructor(private fb: FormBuilder, private netPresentValueService: NetPresentValueService) { }

  ngOnInit() {
    this.netPresentValueForm = this.fb.group({
      rateGroup: this.fb.group({
        lowerBoundDiscountRate: ['', [Validators.required, Validators.pattern(this.positiveNumberRegex)]],
        upperBoundDiscountRate: ['', [Validators.required, Validators.pattern(this.positiveNumberRegex)]],
        discountRateIncrement: ['', [Validators.required, Validators.pattern(this.positiveNumberRegex)]],
      }, {
          validator: discountRateChecker
        }),
      lifeOfProject: ['', [Validators.required]],
      initialCost: ['', [Validators.required, Validators.max(0), Validators.pattern(this.negatativeNumberRegex)]],
      cashFlows: this.fb.array([]),
      results: this.fb.array([])
    });
  }

  addCashFlows(): void {
    this.cashFlows.controls = [];
    for (var i = 0; i < this.netPresentValueForm.get('lifeOfProject').value; i++) {
      this.cashFlows.push(new FormControl('', [Validators.required, Validators.pattern(this.numberRegex)]));
    }
  }

  buildResults(): FormGroup {
    return this.fb.group({
      netPresentValue: '',
      presentValueOfExpectedCashFlows: ''
    });
  }

  calculate(): void {
    if (this.netPresentValueForm.valid) {
      if (this.netPresentValueForm.dirty) {
        this.netPresentValueParam = new NetPresentValueParam();
        this.netPresentValueParam.upperBoundDiscountRate = parseFloat(this.netPresentValueForm.get('rateGroup.upperBoundDiscountRate').value) / 100; // convert to precent
        this.netPresentValueParam.lowerBoundDiscountRate = parseFloat(this.netPresentValueForm.get('rateGroup.lowerBoundDiscountRate').value) / 100; // convert to precent
        this.netPresentValueParam.discountRateIncrement = parseFloat(this.netPresentValueForm.get('rateGroup.discountRateIncrement').value) / 100; // convert to precent
        this.netPresentValueParam.lifeOfProject = this.netPresentValueForm.get('lifeOfProject').value;
        this.netPresentValueParam.initialCost = this.netPresentValueForm.get('initialCost').value;
        this.netPresentValueParam.cashFlows = this.netPresentValueForm.get('cashFlows').value;

        this.netPresentValueService.calculateNetPresentValue(this.netPresentValueParam)
          .subscribe(data => {
            this.netPresentValueResults = data,
              this.buildChartContainer(),
              this.netPresentValueService.createNetPresentValues(this.mapNetPresentValues(data))
                .subscribe(data2 => {
                  this.buildChart(),
                  console.log('save successful'),
                  (error: any) => this.errorMessage = <any>error
                }),
              (error: any) => this.errorMessage = <any>error
          });
      }
    } else {

      this.errorMessage = 'Please correct the validation errors.';
    }
  }

  buildChartContainer() {
    var netPresentValueResults = this.netPresentValueResults;
    var resultCount = [];
      for (let index = 0; index < netPresentValueResults.length; index++) {
        resultCount.push(index);
      }
      this.resultCount = resultCount;
  }

  buildChart() {
    var netPresentValueResults = this.netPresentValueResults;
    var options = this.buildChartOptions(netPresentValueResults);
      for (let index = 0; index < options.length; index++) {
        const option = options[index];
        Highcharts.chart(option['chartContainerId'], option);
      }
  }

  buildChartOptions(netPresentValueResults: NetPresentValueResult[]): any[] {
    var chartOptions = [];
    for (let index = 0; index < netPresentValueResults.length; index++) {
      const element = netPresentValueResults[index];
      var i = index;
      var option = {
        chart: {
          type: 'column'
        },
        title: {
          text: 'Present Value vs. Discounted Value'
        },
        yAxis: {
          title: {
            text: ''
          }
        },
        legend: {
          layout: 'horizontal',
          align: 'center',
          verticalAlign: 'bottom'
        },
        plotOptions: {
          series: {
            label: {
              connectorAllowed: false
            },
            pointStart: 1
          }
        },
        series: [{
          name: 'Present Value',
          data: element.presentValues
        }, {
          name: 'Discounted Value',
          data: element.discountedValues
        }],
        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
              }
            }
          }]
        },
        chartContainerId: 'chartContainerId' + (i + 1)
      }

      chartOptions.push(option);
    }
    return chartOptions;
  }

  mapNetPresentValues(netPresentValueResults: NetPresentValueResult[]): NetPresentValue[] {
    var netPresentValues = [];
    for (var i = 0; i < netPresentValueResults.length; i++) {
      const element = netPresentValueResults[i];
      var netPresentValue = new NetPresentValue();
      var cashFlows = this.netPresentValueForm.get('cashFlows').value;
      netPresentValue.discountRate = element.discountRate;
      netPresentValue.value = element.value;
      netPresentValue.presentValue = element.presentValue;
      netPresentValue.upperBoundDiscountRate = this.netPresentValueForm.get('rateGroup.upperBoundDiscountRate').value;
      netPresentValue.lowerBoundDiscountRate = this.netPresentValueForm.get('rateGroup.lowerBoundDiscountRate').value;
      netPresentValue.discountRateIncrement = this.netPresentValueForm.get('rateGroup.discountRateIncrement').value;
      netPresentValue.lifeOfProject = this.netPresentValueForm.get('lifeOfProject').value;;
      netPresentValue.initialCost = this.netPresentValueForm.get('initialCost').value;
      netPresentValue.cashFlowYear1 = cashFlows[0] != undefined && cashFlows[0] != null ? cashFlows[0] : 0;
      netPresentValue.cashFlowYear2 = cashFlows[1] != undefined && cashFlows[1] != null ? cashFlows[1] : 0;
      netPresentValue.cashFlowYear3 = cashFlows[2] != undefined && cashFlows[2] != null ? cashFlows[2] : 0;
      netPresentValue.cashFlowYear4 = cashFlows[3] != undefined && cashFlows[3] != null ? cashFlows[3] : 0;
      netPresentValue.cashFlowYear5 = cashFlows[4] != undefined && cashFlows[4] != null ? cashFlows[4] : 0;
      netPresentValue.cashFlowYear6 = cashFlows[5] != undefined && cashFlows[5] != null ? cashFlows[5] : 0;
      netPresentValue.cashFlowYear7 = cashFlows[6] != undefined && cashFlows[6] != null ? cashFlows[6] : 0;
      netPresentValue.cashFlowYear8 = cashFlows[7] != undefined && cashFlows[7] != null ? cashFlows[7] : 0;
      netPresentValue.cashFlowYear9 = cashFlows[8] != undefined && cashFlows[8] != null ? cashFlows[8] : 0;
      netPresentValue.cashFlowYear10 = cashFlows[9] != undefined && cashFlows[9] != null ? cashFlows[9] : 0;
      netPresentValue.cashFlowYear11 = cashFlows[10] != undefined && cashFlows[10] != null ? cashFlows[10] : 0;
      netPresentValue.cashFlowYear12 = cashFlows[11] != undefined && cashFlows[11] != null ? cashFlows[11] : 0;
      netPresentValue.cashFlowYear13 = cashFlows[12] != undefined && cashFlows[12] != null ? cashFlows[12] : 0;
      netPresentValue.cashFlowYear14 = cashFlows[13] != undefined && cashFlows[13] != null ? cashFlows[13] : 0;
      netPresentValue.cashFlowYear15 = cashFlows[14] != undefined && cashFlows[14] != null ? cashFlows[14] : 0;
      netPresentValue.cashFlowYear16 = cashFlows[15] != undefined && cashFlows[15] != null ? cashFlows[15] : 0;
      netPresentValue.cashFlowYear17 = cashFlows[16] != undefined && cashFlows[16] != null ? cashFlows[16] : 0;
      netPresentValue.cashFlowYear18 = cashFlows[17] != undefined && cashFlows[17] != null ? cashFlows[17] : 0;
      netPresentValue.cashFlowYear19 = cashFlows[18] != undefined && cashFlows[18] != null ? cashFlows[18] : 0;
      netPresentValue.cashFlowYear20 = cashFlows[19] != undefined && cashFlows[19] != null ? cashFlows[19] : 0;
      netPresentValues.push(netPresentValue);
    }
    return netPresentValues;
  }
}
