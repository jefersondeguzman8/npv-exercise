using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using NPV.BAL.Calculator;
using NPV.Models.Response;
using NPV.Models.Web;

namespace NPV.Angular.Web.Controllers
{
    [Route("api/[controller]")]
    public class NetPresentValuesController : Controller
    {
        [HttpPost("[action]")]
        public IEnumerable<NetPresentValueResult> CalculateNetPresentValue([FromBody] NetPresentValueParam netPresentValueParam)
        {
            return NetPresentValueCalculator.Calculate(netPresentValueParam);
        }
    }
}
