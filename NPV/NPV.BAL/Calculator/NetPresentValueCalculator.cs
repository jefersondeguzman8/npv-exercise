﻿using NPV.Models.Response;
using NPV.Models.Web;
using System;
using System.Collections.Generic;

namespace NPV.BAL.Calculator
{
    public class NetPresentValueCalculator
    {
        public static IEnumerable<NetPresentValueResult> Calculate(NetPresentValueParam netPresentValueParam)
        {
            if (netPresentValueParam != null)
            {
                var listOfNetPresentValueResult = new List<NetPresentValueResult>();

                var lowerBoundDiscountRate = Math.Round(netPresentValueParam.LowerBoundDiscountRate, 4, MidpointRounding.AwayFromZero);
                var discountRateIncrement = Math.Round(netPresentValueParam.DiscountRateIncrement, 4, MidpointRounding.AwayFromZero);
                var upperBoundDiscountRate = netPresentValueParam.UpperBoundDiscountRate;               

                double totalPresentValue;
                double netPresentValue;

                for (double discountRate = lowerBoundDiscountRate; discountRate <= upperBoundDiscountRate; discountRate += discountRateIncrement)
                {
                    discountRate = Math.Round(discountRate, 4, MidpointRounding.AwayFromZero);

                    var netPresentValueresult = new NetPresentValueResult();

                    netPresentValueresult.DiscountFactors = new List<double>();
                    netPresentValueresult.PresentValues = new List<double>();
                    netPresentValueresult.DiscountedValues = new List<double>();

                    var presentValues = new List<double>();
                    var discountedValues = new List<double>();

                    totalPresentValue = 0;
                    netPresentValue = 0;

                    for (short yearLifeOfProject = 1; yearLifeOfProject <= netPresentValueParam.LifeOfProject; yearLifeOfProject++)
                    {
                        var discountFactor = GetDiscountFactor(discountRate, yearLifeOfProject);
                        var undisCountedCashFlow = netPresentValueParam.CashFlows[yearLifeOfProject - 1];
                        var presentValue = GetPresentValue(discountFactor, undisCountedCashFlow);
                        var discountedValue = GetDiscountedValue(undisCountedCashFlow, presentValue);
                        totalPresentValue += presentValue;

                        netPresentValueresult.DiscountFactors.Add(discountFactor);
                        netPresentValueresult.PresentValues.Add(Math.Round(presentValue, 2));
                        netPresentValueresult.DiscountedValues.Add(Math.Round(discountedValue, 2));
                    }

                    netPresentValue = GetNetPresentValue(totalPresentValue, netPresentValueParam.InitialCost);

                    netPresentValueresult.DiscountRate = discountRate;
                    netPresentValueresult.Value = Math.Round(netPresentValue, 2);
                    netPresentValueresult.PresentValue = Math.Round(totalPresentValue, 2);

                    listOfNetPresentValueResult.Add(netPresentValueresult);
                }

                return listOfNetPresentValueResult;
            }
            else
            {
                return null;
            }           
        }

        public static double GetDiscountFactor(double discountRate, short yearLifeOfProject)
        {
            return 1 / Math.Pow((1 + discountRate), yearLifeOfProject);
        }

        public static double GetPresentValue(double discountFactor, double undisCountedCashFlow)
        {
            return discountFactor * undisCountedCashFlow;
        }

        public static double GetDiscountedValue(double undiscountedCashFlow, double presentValue)
        {
            return undiscountedCashFlow - presentValue;
        }

        public static double GetNetPresentValue(double totalPresentValue, double initialCost)
        {
            return totalPresentValue - initialCost;
        }
    }
}
