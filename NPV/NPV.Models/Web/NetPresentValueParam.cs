﻿using System;

namespace NPV.Models.Web
{
    public class NetPresentValueParam
    {
        public double LowerBoundDiscountRate { get; set; }
        public double UpperBoundDiscountRate { get; set; }
        public double DiscountRateIncrement { get; set; }
        public short LifeOfProject { get; set; }
        public double InitialCost { get; set; }
        public double[] CashFlows { get; set; }      
    }
}
