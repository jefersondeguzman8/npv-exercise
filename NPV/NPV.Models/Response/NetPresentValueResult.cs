﻿using System.Collections.Generic;

namespace NPV.Models.Response
{
    public class NetPresentValueResult
    {
        public double DiscountRate { get; set; }
        public List<double> DiscountFactors { get; set; }
        public List<double> PresentValues { get; set; }
        public double Value { get; set; }
        public double PresentValue { get; set; }
        public List<double> DiscountedValues { get; set; }
    }
}
