using NPV.Angular.Web.Controllers;
using NPV.Models.Response;
using NPV.Models.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NPV.Angular.Web.Controller.Test
{
    public class NetPresentValuesControllerTest
    {
        [Fact]
        public void CalculateNetPresentValue_Should_Return_Correct_PresentValue_And_NetPresentValue()
        {
            var controller = new NetPresentValuesController();

            double[] cashFlows = new double[10];
            cashFlows[0] = 10000;
            cashFlows[1] = 10000;
            cashFlows[2] = 10000;
            cashFlows[3] = 10000;
            cashFlows[4] = 10000;
            cashFlows[5] = 10000;
            cashFlows[6] = 10000;
            cashFlows[7] = 10000;
            cashFlows[8] = 10000;
            cashFlows[9] = 10000;

            var netPresentValueParam = new NetPresentValueParam()
            {
                LowerBoundDiscountRate = 0.01,
                UpperBoundDiscountRate = 0.10,
                LifeOfProject = 10,
                DiscountRateIncrement = 0.01,
                InitialCost = 0,
                CashFlows = cashFlows
            };

            List<NetPresentValueResult> result = controller.CalculateNetPresentValue(netPresentValueParam).ToList();

            Assert.Equal(61445.67, result[9].Value);
            Assert.Equal(61445.67, result[9].PresentValue);
        }

        [Fact]
        public void CalculateNetPresentValue_Should_Return_Correct_PresentValue_And_NetPresentValue_With_Randdom_Cashflows()
        {
            var controller = new NetPresentValuesController();

            double[] cashFlows = new double[10];
            cashFlows[0] = 897;
            cashFlows[1] = 9000;
            cashFlows[2] = 454;
            cashFlows[3] = 8987;
            cashFlows[4] = 654.45;
            cashFlows[5] = 987;
            cashFlows[6] = 41654.11;
            cashFlows[7] = 879;
            cashFlows[8] = 132;
            cashFlows[9] = 564;

            var netPresentValueParam = new NetPresentValueParam()
            {
                LowerBoundDiscountRate = 0.01,
                UpperBoundDiscountRate = 0.01,
                LifeOfProject = 10,
                DiscountRateIncrement = 0.01,
                InitialCost = 10000,
                CashFlows = cashFlows
            };

            List<NetPresentValueResult> result = controller.CalculateNetPresentValue(netPresentValueParam).ToList();

            Assert.Equal(50634.80, result[0].Value);
            Assert.Equal(60634.80, result[0].PresentValue);
        }

        [Fact]
        public void CalculateNetPresentValue_Should_Return_Two_Records_When_LowerRate_Is_One_And_UpperRate_Is_Five_And_IncrementRate_Is_Two()
        {
            var controller = new NetPresentValuesController();

            double[] cashFlows = new double[1];
            cashFlows[0] = 10000;

            var netPresentValueParam = new NetPresentValueParam()
            {
                LowerBoundDiscountRate = 0.01,
                UpperBoundDiscountRate = 0.05,
                LifeOfProject = 1,
                DiscountRateIncrement = 0.02,
                InitialCost = 0,
                CashFlows = cashFlows
            };

            List<NetPresentValueResult> result = controller.CalculateNetPresentValue(netPresentValueParam).ToList();

            Assert.Equal(3, result.Count());
        }

        [Fact]
        public void CalculateNetPresentValue_Should_Return_Five_Records_When_LowerRate_Is_One_And_UpperRate_Is_Five_And_IncrementRate_Is_One()
        {
            var controller = new NetPresentValuesController();

            double[] cashFlows = new double[1];
            cashFlows[0] = 10000;

            var netPresentValueParam = new NetPresentValueParam()
            {
                LowerBoundDiscountRate = 0.01,
                UpperBoundDiscountRate = 0.05,
                LifeOfProject = 1,
                DiscountRateIncrement = 0.01,
                InitialCost = 0,
                CashFlows = cashFlows
            };

            List<NetPresentValueResult> result = controller.CalculateNetPresentValue(netPresentValueParam).ToList();

            Assert.Equal(5, result.Count());
        }

        [Fact]
        public void CalculateNetPresentValue_Should_Return_6_Items_When_LowerRate_Is_3point65_UpperRate_Is_3point70_And_IncrementRate_Is_0point01()
        {
            var controller = new NetPresentValuesController();

            double[] cashFlows = new double[5];
            cashFlows[0] = 10000;
            cashFlows[1] = 10000;
            cashFlows[2] = 10000;
            cashFlows[3] = 10000;
            cashFlows[4] = 10000;

            var netPresentValueParam = new NetPresentValueParam()
            {
                LowerBoundDiscountRate = 3.65 / 100,
                UpperBoundDiscountRate = 3.70 / 100,
                LifeOfProject = 5,
                DiscountRateIncrement = 0.01 / 100,
                InitialCost = -10000,
                CashFlows = cashFlows
            };

            List<NetPresentValueResult> result = controller.CalculateNetPresentValue(netPresentValueParam).ToList();

            Assert.Equal(6, result.Count());
        }
    }
}
